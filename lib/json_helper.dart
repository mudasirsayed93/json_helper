library json_helper;

import 'dart:convert';

/// A Calculator.
class JsonHelper {

  /*
  * @method: getInt
  * @params: dynamic anyNumber
  * return value in int format or 0
  * */
  static int getInt(dynamic anyNumber){
    if(anyNumber != null){
      return  (( anyNumber is String)) ? ( int.tryParse( anyNumber ) ?? 0) : anyNumber;
    }
    return 0;
  }

  /*
  * @method: getDouble
  * @params: dynamic anyNumber
  * return value in double format or 0.0
  * */
  static double getDouble(dynamic anyNumber){
    return (anyNumber == null) ? 0.0 : double.parse('$anyNumber') ;
  }

  /*
  * @method: getDouble
  * @params: dynamic anyString
  * return value in String format or emptry
  * */
  static String getString(dynamic anyString){
    return (anyString != null)?  anyString.toString()  : "";
  }

  /*
  * @method: getBool
  * @params: dynamic anyBool
  * return value in bool format or false
  * */
  static bool getBool(dynamic anyBool){
    if(anyBool is bool)  {
      return anyBool;
    } else {
      return (( anyBool is int)) ?  ((anyBool == 1 )? true : false)  : false;
    }
  }

  /*
  * @method: getDynamicList
  * @params: anyData
  * return value in List<dynamic> format or empty array
  * */
  static List<dynamic> getDynamicList(  anyData ){
    return ( anyData is List<dynamic>) ? anyData : [];
  }

  /*
  * @method: getDynamicMap
  * @params: anyData
  * return value in Map<String, dynamic>  format or empty object
  * */
  static Map<String, dynamic> getDynamicMap( anyData ){
    return ( anyData is Map<String, dynamic>) ? anyData : {};
  }

  /*
  * @method: getDateTime
  * @params: anyDate
  * return value in DateTime format or cuttent datetime
  * */
  static DateTime getDateTime(dynamic anyDate){
    return (anyDate != null && anyDate != '')?  DateTime.parse(anyDate) : DateTime.now();
  }

  /*
  * @method: base64Encoded
  * @params: string
  * return value in encoded base64
  * */
  static Future<String> base64Encoded(String string) async {
    Codec<String, String> stringToBase64 = utf8.fuse(base64);
    return stringToBase64.encode(string);
  }

  /*
  * @method: base64Decoded
  * @params: string
  * return value in decoded base64
  * */
  static Future<String> base64Decoded (String string) async {
    Codec<String, String> stringToBase64 = utf8.fuse(base64);
    return stringToBase64.decode(string);
  }

  /*
  * @method: uCFirst
  * @params: string
  * return value in UCFirst format
  * */
  static String uCFirst(String string) {
    return (string.isNotEmpty) ? string[0].toUpperCase() + string.substring(1) : '';
  }

}
